class CreateDeals < ActiveRecord::Migration
  def change
    create_table :deals do |t|
      t.string :title
      t.text :description
      t.decimal :price, precision: 8, scale: 2
      t.decimal :discounted_price, precision: 8, scale: 2
      t.integer :quantity
      t.date :publish_date
      t.string :image_name
      t.string :image_type
      t.boolean :expired, default: false

      t.timestamps null: false
    end
  end
end
