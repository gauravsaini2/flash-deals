class Deal < ActiveRecord::Base
  #VALIDATIONS
  validates :title, :description, :price, :discounted_price, :quantity, :publish_date, :image_path, presence: true
  validates :publish_date, uniqueness: {message: "Already exists an deal to be published on this date. Please edit publish date."}

  #PUBLIC METHODS
  def image=(image_file)
    file_type = image_file.content_type
    if file_type == 'image/jpeg' || file_type == 'image/jpg' || file_type == 'image/png' || file_type == 'image/gif' || file_type == 'image/bmp'
      # as per the file type give the images name
      case file_type
        when "image/jpeg"
          file_name = "pic_#{Time.now.to_i}.jpg"
        when "image/png"
          file_name = "pic_#{Time.now.to_i}.png"
        when "image/gif"
          file_name = "pic_#{Time.now.to_i}.gif"
        when "image/bmp"
          file_name = "pic_#{Time.now.to_i}.bmp"
      end
      file_path = File.join(Rails.root, 'public', 'images', 'upload_images', file_name)
      File.open(file_path, 'wb') do |f|
        f.write image_file.read
      end
      self.image_type = file_type
      self.image_name = file_name
    else
      self.errors.add(:image_type, "should be one of jpeg/bmp/png/gif.")
    end
  end

end
