class DealsController < ApplicationController
  before_action :admin_logged_in?
  before_action :user_logged_in?

  def index
    @deal = Deal.first
  end

  private
  def user_logged_in?
    redirect_to new_user_session_url if current_user.present?
  end

  def admin_logged_in?
    redirect_to admins_url if current_admin.present?
  end
end
