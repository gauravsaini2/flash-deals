class AdminsController < ApplicationController
  before_action :admin_logged_in?

  def index

  end

  private
  def admin_logged_in?
    redirect_to root_url unless current_admin.present?
  end

end
